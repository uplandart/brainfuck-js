'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(function () {
	'use strict';

	function getArrayObjByKeyVal(arr, key, val) {
		for (var i = 0; i < arr.length; i++) {
			var a = arr[i];
			if (a.hasOwnProperty(key) && a[key] === val) {
				return a;
			}
		}
		return null;
	}

	var CUSTOM_OUTPUT_STREAM = 'custom_output_stream';
	var CUSTOM_INPUT_STREAM = 'custom_input_stream';
	var DEFAULT_INIT_SIZE = 300;
	var AVAILABLE_COMMANDS = '<>+-.,[]';

	var BrainfuckJS = function () {
		function BrainfuckJS(args) {
			_classCallCheck(this, BrainfuckJS);

			if (Array.isArray(args)) {
				this._arr = args;
			} else {
				args = parseInt(args, 10) || DEFAULT_INIT_SIZE;
				this._arr = new Array(args);
				this._arr.fill(0);
			}
			this._pos = this._arr.length / 2;
			this._loops = [];
			this._is_run = false;
		}

		// PRIVATE


		_createClass(BrainfuckJS, [{
			key: '_inputStream',
			value: function _inputStream(index) {
				var input = null;
				do {
					input = prompt('Please, enter value:');
					if (!input) {
						if (confirm('Do you want to break execution program?')) {
							this.stop();
							this.clear();
							return null;
						}
					}
					input = Number(input) || null;
				} while (this._is_run && (input == null || input == ""));
				return input;
			}
		}, {
			key: '_outputStream',
			value: function _outputStream(str) {
				console.log(str);
			}
		}, {
			key: '_execCommand',
			value: function _execCommand(command) {
				switch (command) {
					case '>':
						this._pos++;
						this._i++;
						break;
					case '<':
						this._pos--;
						this._i++;
						break;
					case '+':
						this._arr[this._pos]++;
						this._i++;
						break;
					case '-':
						this._arr[this._pos]--;
						this._i++;
						break;
					case '.':
						this._output(String.fromCharCode(this._arr[this._pos]));
						this._i++;
						break;
					case ',':
						var _data = parseInt(this._input(this._i));
						if (this._is_run) {
							if (isNaN(_data) || !isFinite(_data)) throw new Error('Input must be a number value');
							this._arr[this._pos] = _data;
							this._i++;
						}
						break;
					case '[':
						if (this._arr[this._pos] == 0) {
							var r = 0;
							for (var j = this._i + 1, l = this._code.length; j < l; j++) {
								var c = this._code[j];
								if (c == '[') r++;
								if (c == ']' && ! --r) this._i = j;break;
							}
						} else {
							var obj = null;
							if (obj = getArrayObjByKeyVal(this._loops, 'index', this._i)) {
								obj.count++;
							} else {
								this._loops.push({ index: this._i, count: 1 });
							}
							this._i++;
						}
						break;
					case ']':
						if (this._arr[this._pos] != 0) {
							this._i = this._loops[this._loops.length - 1].index;
						} else {
							this._loops.splice(this._loops.length - 1, 1);
							this._i++;
						}
						break;
					default:
						throw new BrainfuckJSCommandException('unknown command "' + command + '"', command, this._i);
				}
			}

			// SETTINGS

		}, {
			key: 'setTape',
			value: function setTape(arr) {
				if (!Array.isArray(arr)) throw new Error('Tape must be an array');
				this._arr = arr;
				this._pos = this._arr.length / 2;
			}
		}, {
			key: 'setPosition',
			value: function setPosition(pos) {
				this._pos = pos;
			}
		}, {
			key: 'setDefaultOutputStream',
			value: function setDefaultOutputStream() {
				delete this[CUSTOM_OUTPUT_STREAM];
			}
		}, {
			key: 'setDefaultInputStream',
			value: function setDefaultInputStream() {
				delete this[CUSTOM_INPUT_STREAM];
			}
		}, {
			key: 'setInputStream',
			value: function setInputStream(i) {
				if (typeof i !== 'function') throw new Error('Input must be a function');
				this[CUSTOM_INPUT_STREAM] = i;
			}
		}, {
			key: 'setOutputStream',
			value: function setOutputStream(o) {
				if (typeof o !== 'function') throw new Error('Output must be a function');
				this[CUSTOM_OUTPUT_STREAM] = o;
			}
		}, {
			key: 'setDefaultIOStream',
			value: function setDefaultIOStream() {
				this.setDefaultInputStream();
				this.setDefaultOutputStream();
			}
		}, {
			key: 'setIOStream',
			value: function setIOStream(i, o) {
				this.setInputStream(i);
				this.setOutputStream(o);
			}
		}, {
			key: 'run',
			value: function run(code) {
				var _ref;

				if (!code) throw new Error('Parameter code necessary');
				BrainfuckJS.checkCodeCommands(code);

				var self = this;
				this._is_run = true;
				this._code = code;
				this._input = this.hasOwnProperty(CUSTOM_INPUT_STREAM) ? this[CUSTOM_INPUT_STREAM] : this._inputStream;
				this._output = this.hasOwnProperty(CUSTOM_OUTPUT_STREAM) ? this[CUSTOM_OUTPUT_STREAM] : this._outputStream;
				this._i = 0;

				return _ref = {}, _defineProperty(_ref, Symbol.iterator, function () {
					return this;
				}), _defineProperty(_ref, 'next', function next() {
					if (!self._is_run) {
						return {
							done: true
						};
					}
					if (self._i < self._code.length) {
						self._execCommand(self._code[self._i]);
						return {
							done: false,
							value: {
								i: self._i,
								pos: self._pos,
								val: self._arr[self._pos]
							}
						};
					} else {
						this._is_run = false;
						return {
							done: true
						};
					}
				}), _ref;
			}
		}, {
			key: 'stop',
			value: function stop() {
				if (this._is_run) {
					this._is_run = false;
				}
			}
		}, {
			key: 'exec',
			value: function exec(code) {
				var _iteratorNormalCompletion = true;
				var _didIteratorError = false;
				var _iteratorError = undefined;

				try {
					for (var _iterator = this.run(code)[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
						var res = _step.value;

						if (!this._is_run) break;
					}
				} catch (err) {
					_didIteratorError = true;
					_iteratorError = err;
				} finally {
					try {
						if (!_iteratorNormalCompletion && _iterator.return) {
							_iterator.return();
						}
					} finally {
						if (_didIteratorError) {
							throw _iteratorError;
						}
					}
				}
			}
		}, {
			key: 'clear',
			value: function clear() {
				this._arr.fill(0);
				this._pos = this._arr.length / 2;
				this._loops = [];
			}
		}, {
			key: 'execAndClear',
			value: function execAndClear(code) {
				this.exec(code);
				this.stop();
				this.clear();
			}
		}, {
			key: 'getTapeLength',


			// GET DATA
			value: function getTapeLength() {
				return this._arr.length;
			}
		}, {
			key: 'getCurrTapeVal',
			value: function getCurrTapeVal() {
				return this._arr[this._pos];
			}
		}, {
			key: 'getPosition',
			value: function getPosition() {
				return this._pos;
			}
		}, {
			key: 'getCodePos',
			value: function getCodePos() {
				return this._i;
			}
		}, {
			key: 'getCurrLoop',
			value: function getCurrLoop() {
				return this._loops[this._loops.length - 1];
			}
		}, {
			key: 'isRun',
			value: function isRun() {
				return this._is_run;
			}
		}], [{
			key: 'checkCodeCommands',


			// EXECUTION
			value: function checkCodeCommands(code) {
				for (var i = 0, len = code.length; i < len; i++) {
					var command = code[i];
					if (AVAILABLE_COMMANDS.indexOf(command) < 0) throw new BrainfuckJSCommandException('unknown command "' + command + '"', command, i);
				}
			}
		}]);

		return BrainfuckJS;
	}();

	var BrainfuckJSCommandException = function (_Error) {
		_inherits(BrainfuckJSCommandException, _Error);

		function BrainfuckJSCommandException(message, char, index) {
			_classCallCheck(this, BrainfuckJSCommandException);

			var _this = _possibleConstructorReturn(this, (BrainfuckJSCommandException.__proto__ || Object.getPrototypeOf(BrainfuckJSCommandException)).call(this, message));

			_this.message = message;
			_this.char = char;
			_this.index = index;
			return _this;
		}

		return BrainfuckJSCommandException;
	}(Error);

	window.BrainfuckJS = BrainfuckJS;
	window.BrainfuckJSCommandException = BrainfuckJSCommandException;
})();