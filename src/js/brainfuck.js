(function () {
	'use strict';

	function getArrayObjByKeyVal(arr, key, val) {
		for (let i = 0; i < arr.length; i++) {
			let a = arr[i];
			if (a.hasOwnProperty(key) && a[key] === val) {
				return a;
			}
		}
		return null;
	}

	const CUSTOM_OUTPUT_STREAM = 'custom_output_stream';
	const CUSTOM_INPUT_STREAM = 'custom_input_stream';
	const DEFAULT_INIT_SIZE = 300;
	const AVAILABLE_COMMANDS = '<>+-.,[]';

	class BrainfuckJS {
		constructor(args) {
			if (Array.isArray(args)) {
				this._arr = args;
			} else {
				args = parseInt(args, 10) || DEFAULT_INIT_SIZE;
				this._arr = new Array(args);
				this._arr.fill(0);
			}
			this._pos = this._arr.length / 2;
			this._loops = [];
			this._is_run = false;
		}

		// PRIVATE
		_inputStream(index) {
			let input = null;
			do {
				input = prompt('Please, enter value:');
				if (!input) {
					if (confirm('Do you want to break execution program?')) {
						this.stop();
						this.clear();
						return null;
					}
				}
				input = Number(input) || null;
			} while( this._is_run && (input == null || input == "") );
			return input;
		}
		_outputStream(str) {
			console.log(str);
		}

		_execCommand(command) {
			switch (command) {
				case '>':
					this._pos++;
					this._i++;
					break;
				case '<':
					this._pos--;
					this._i++;
					break;
				case '+':
					this._arr[this._pos]++;
					this._i++;
					break;
				case '-':
					this._arr[this._pos]--;
					this._i++;
					break;
				case '.':
					this._output(String.fromCharCode(this._arr[this._pos]));
					this._i++;
					break;
				case ',':
					let _data = parseInt(this._input(this._i));
					if (this._is_run) {
						if (isNaN(_data) || !isFinite(_data)) throw new Error('Input must be a number value');
						this._arr[this._pos] = _data;
						this._i++;
					}
					break;
				case '[':
					if (this._arr[this._pos] == 0) {
						let r = 0;
						for (let j = this._i+1, l = this._code.length; j < l; j++) {
							let c = this._code[j];
							if (c == '[') r++;
							if (c == ']' && !--r) this._i = j; break;
						}
					} else {
						let obj = null;
						if (obj = getArrayObjByKeyVal(this._loops, 'index', this._i)) {
							obj.count++;
						} else {
							this._loops.push({ index: this._i, count: 1 });
						}
						this._i++;
					}
					break;
				case ']':
					if (this._arr[this._pos] != 0) {
						this._i = this._loops[this._loops.length-1].index;
					} else {
						this._loops.splice(this._loops.length-1, 1);
						this._i++;
					}
					break;
				default:
					throw new BrainfuckJSCommandException('unknown command "'+command+'"', command, this._i);
			}
		}

		// SETTINGS
		setTape(arr) {
			if (!Array.isArray(arr)) throw new Error('Tape must be an array');
			this._arr = arr;
			this._pos = this._arr.length / 2;
		};
		setPosition(pos) {
			this._pos = pos;
		}

		setDefaultOutputStream() {
			delete this[CUSTOM_OUTPUT_STREAM];
		};
		setDefaultInputStream() {
			delete this[CUSTOM_INPUT_STREAM];
		};

		setInputStream(i) {
			if (typeof i !== 'function') throw new Error('Input must be a function');
			this[CUSTOM_INPUT_STREAM] = i;
		};
		setOutputStream(o) {
			if (typeof o !== 'function') throw new Error('Output must be a function');
			this[CUSTOM_OUTPUT_STREAM] = o;
		};

		setDefaultIOStream() {
			this.setDefaultInputStream();
			this.setDefaultOutputStream();
		};
		setIOStream(i, o) {
			this.setInputStream(i);
			this.setOutputStream(o);
		};

		// EXECUTION
		static checkCodeCommands(code) {
			for (let i = 0, len = code.length; i < len; i++) {
				let command = code[i];
				if (AVAILABLE_COMMANDS.indexOf(command) < 0)
					throw new BrainfuckJSCommandException('unknown command "' + command + '"', command, i);
			}
		}

		run(code) {
			if (!code) throw new Error('Parameter code necessary');
			BrainfuckJS.checkCodeCommands(code);

			var self = this;
			this._is_run = true;
			this._code = code;
			this._input = this.hasOwnProperty(CUSTOM_INPUT_STREAM) ? this[CUSTOM_INPUT_STREAM] : this._inputStream;
			this._output = this.hasOwnProperty(CUSTOM_OUTPUT_STREAM) ? this[CUSTOM_OUTPUT_STREAM] : this._outputStream;
			this._i = 0;

			return {
				[Symbol.iterator]() {
					return this;
				},
				next() {
					if (!self._is_run) {
						return {
							done: true
						}
					}
					if (self._i < self._code.length) {
						self._execCommand(self._code[self._i]);
						return {
							done: false,
							value: {
								i: self._i,
								pos: self._pos,
								val: self._arr[self._pos]
							}
						}
					} else {
						this._is_run = false;
						return {
							done: true
						}
					}
				}
			};
		}
		stop() {
			if (this._is_run) {
				this._is_run = false;
			}
		}

		exec(code) {
			for (let res of this.run(code)) {
				if (!this._is_run) break;
			}
		};

		clear() {
			this._arr.fill(0);
			this._pos = this._arr.length / 2;
			this._loops = [];
		};
		execAndClear(code) {
			this.exec(code);
			this.stop();
			this.clear();
		};

		// GET DATA
		getTapeLength() {
			return this._arr.length;
		};
		getCurrTapeVal() {
			return this._arr[this._pos];
		};
		getPosition() {
			return this._pos;
		};
		getCodePos() {
			return this._i;
		}
		getCurrLoop() {
			return this._loops[this._loops.length-1];
		}
		isRun() {
			return this._is_run;
		}
	}

	class BrainfuckJSCommandException extends Error {
		constructor(message, char, index) {
			super(message);
			this.message = message;
			this.char = char;
			this.index = index;
		}
	}

	window.BrainfuckJS = BrainfuckJS;
	window.BrainfuckJSCommandException = BrainfuckJSCommandException;
})();