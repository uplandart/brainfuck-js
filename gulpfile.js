'use strict';

const gulp = require('gulp')

		, gBabel = require('gulp-babel')
		, gUglify = require('gulp-uglify')

		, gConcat = require('gulp-concat')
		, gRename = require('gulp-rename');

const path = require('path');

const PATH_TO_SRC_DIR = path.join(__dirname, 'src');
const PATH_TO_SRC_JS_DIR = path.join(PATH_TO_SRC_DIR, 'js');

const PATH_TO_DIST_DIR = path.join(__dirname, 'dist');

const DIST_FILENAME = 'brainfuck';

gulp.task('compile_js', compile_js);
gulp.task('compile_js_min', ['compile_js'], compile_js_min);

gulp.task('build', [
	'compile_js'
], () => {});
gulp.task('build_min', [
	'compile_js_min'
], () => {});

gulp.task('watch_js', watch_js);
gulp.task('watch_js_min', watch_js_min);

gulp.task('watch', [
	'watch_js'
], () => {});
gulp.task('watch_min', [
	'watch_js_min'
], () => {});

gulp.task('listen', ['build', 'watch']);
gulp.task('listen_min', ['build_min', 'watch_min']);
gulp.task('default', ['build']);
gulp.task('default_min', ['build_min']);

// COMPILE TASKS FUNCTIONS
function compile_js() {
	return gulp.src(path.join(PATH_TO_SRC_JS_DIR, '**', '*.js'))
			.pipe(gBabel({
				presets: ['es2015']
			}))
			.pipe(gConcat(DIST_FILENAME + '.js'))
			.pipe(gulp.dest(PATH_TO_DIST_DIR));
}
function compile_js_min() {
	return gulp.src(path.join(PATH_TO_DIST_DIR, DIST_FILENAME + '.js'))
			.pipe(gUglify())
			.pipe(gRename(DIST_FILENAME + '.min.js'))
			.pipe(gulp.dest(PATH_TO_DIST_DIR));
}

// WATCH TASKS FUNCTIONS
function watchWatcher(watcher, title) {
	watcher.on('change', (event) => {
		console.log(`${title} "${event.type}" in path: ${event.path}`);
	});
}

function watch_js() {
	let watcher = gulp.watch(path.join(PATH_TO_SRC_JS_DIR, '**', '*.js'), ['compile_js']);
	watchWatcher(watcher,'Watch js');
}
function watch_js_min() {
	let watcher = gulp.watch(path.join(PATH_TO_SRC_JS_DIR, '**', '*.js'), ['compile_js_min']);
	watchWatcher(watcher,'Watch js with min');
}

// FOR npm run
let devmode = 0;
let watch = 0;
let argv = process.argv;
if (argv.length > 2) {
	if (argv[2] === 'development') {
		devmode = 1;
	}
	if (argv.length > 3) watch = 1;
}
console.log("MODE", devmode ? 'development' : 'production');
if (watch) console.log('with watch changes');

if (watch) {
	console.log('Start watch src directory');
	if (devmode) gulp.start('listen');
	else gulp.start('listen_min');
} else {
	console.log('Please, wait for compile project');
	if (devmode) gulp.start('default');
	else gulp.start('default_min');
}